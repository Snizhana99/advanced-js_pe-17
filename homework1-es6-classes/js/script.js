class Employee {
  constructor(name, age, salary) {
    this.name = name;
    this.age = age;
    this._salary = salary;
  }
  get name() {
    return this.name;
  }
  set name(name) {
    return (this._name = name);
  }
  get age() {
    return this.age;
  }
  set age(age) {
    return (this._age = age);
  }
  get salary() {
    return this.salary;
  }
  set salary(salary) {
    return (this._salary = salary);
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }
  get salary() {
    return this._salary * 3;
  }
}

let employee = new Employee("Sofi", "25", 1000);
console.log(employee);

let programmer = new Programmer("Ivan", "35", 1000, "JS");
console.log(programmer);
console.log(programmer.salary);

let programmer2 = new Programmer("Sergij", "28", 6000, "C++");
console.log(programmer2);
console.log(programmer2.salary);

let programmer3 = new Programmer("Mykola", "27", 2000, "Python");
console.log(programmer3);
console.log(programmer3.salary);
