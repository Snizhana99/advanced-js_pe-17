## Теоретичне питання

1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript

Наслідування — процес, що дозволяє одному об'єкту базуватися на іншому. Такий підхід дозволяє об'єктам розділяти спільні властивості.

Прототипне наслідування в JavaScript - це механізм, за допомогою якого один об'єкт може успадковувати властивості і методи іншого об'єкта. У JavaScript об'єкти мають внутрішній посилання на інші об'єкти, які вони використовують для успадкування.

Коли ми створюємо новий об'єкт в JavaScript, ми можемо встановити його прототип - це інший об'єкт, який використовується для пошуку властивостей і методів, які не знайдено в самому об'єкті. Якщо JavaScript не може знайти властивість або метод у поточному об'єкті, він автоматично перевіряє прототипний об'єкт. Цей процес продовжується по ланцюжку прототипів, поки не буде знайдено шукану властивість або метод або не буде досягнуто кінця ланцюжка.

Коли ми викликаємо метод або отримуємо властивість об'єкта, JavaScript спочатку перевіряє, чи присутній цей метод або властивість у самому об'єкті. Якщо так, то він використовує його. Але якщо цей метод або властивість не знайдено, JavaScript перевіряє прототипний об'єкт, і так далі.

Прототипний об'єкт є посиланням на інший об'єкт, і будь-які зміни, зроблені в прототипному об'єкті, відображаються у всіх об'єктах, які успадковують цей прототип. Це означає, що ми можемо додавати нові методи і властивості до прототипу після створення об'єктів, і всі успадковані об'єкти будуть мати доступ до цих нових методів і властивостей.

Одна з важливих особливостей прототипного наслідування в JavaScript полягає в тому, що об'єкт може мати тільки один прототип. Це відрізняє його від класового наслідування, де об'єкт може успадковувати властивості і методи з багатьох класів.

2. Для чого потрібно викликати super() у конструкторі класу-нащадка?

Виклик super() у конструкторі класу-нащадка дозволяє класу-нащадку успадкувати та налаштувати властивості класу-батька.
