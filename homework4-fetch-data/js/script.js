// ## Завдання
// Отримати список фільмів серії `Зоряні війни` та вивести на екран список персонажів для кожного з них.

// #### Технічні вимоги:
// - Надіслати AJAX запит на адресу `https://ajax.test-danit.com/api/swapi/films` та отримати список усіх фільмів серії `Зоряні війни`
// - Для кожного фільму отримати з сервера список персонажів, які були показані у цьому фільмі. Список персонажів можна отримати з властивості `characters`.
// - Як тільки з сервера буде отримана інформація про фільми, відразу вивести список усіх фільмів на екрані. Необхідно вказати номер епізоду, назву фільму, а також короткий зміст (поля `episodeId`, `name`, `openingCrawl`).
// - Як тільки з сервера буде отримано інформацію про персонажів будь-якого фільму, вивести цю інформацію на екран під назвою фільму.

// #### Необов'язкове завдання підвищеної складності
//  - Поки завантажуються персонажі фільму, прокручувати під назвою фільму анімацію завантаження. Анімацію можна використовувати будь-яку. Бажано знайти варіант на чистому CSS без використання JavaScript.
// --------------------------------------

fetch("https://ajax.test-danit.com/api/swapi/films")
  .then((response) => response.json())
  .then((data) => {
    data.sort((a, b) => a.episodeId - b.episodeId);

    const wrap = document.querySelector("#wrapper");
    const filmsList = document.createElement("ul");
    filmsList.classList.add("films-information");
    wrap.appendChild(filmsList);

    function displayFilm(film) {
      const filmItem = document.createElement("li");
      filmItem.classList.add("film-information");
      filmItem.innerHTML = `<h2>Episode ${film.episodeId}: ${film.name}</h2><p>${film.openingCrawl}</p>`;
      filmsList.appendChild(filmItem);
      getCharacters(film.characters, filmItem);
    }
    data.forEach((film) => {
      displayFilm(film);
    });

    function getCharacters(characters, filmItem) {
      filmItem.classList.add("loading");
      Promise.all(
        characters.map((characterUrl) =>
          fetch(characterUrl).then((response) => response.json())
        )
      )
        .then((characters) => {
          filmItem.classList.remove("loading");

          const charactersTitle = document.createElement("h3");
          charactersTitle.innerHTML = "Character list:";
          filmItem.appendChild(charactersTitle);

          const charactersList = document.createElement("ul");
          charactersList.className = "films-characters";
          characters.forEach((character) => {
            const characterItem = document.createElement("li");
            characterItem.classList.add("characters");
            characterItem.textContent = character.name;
            charactersList.appendChild(characterItem);
          });
          filmItem.appendChild(charactersList);
        })
        .catch((error) => {
          console.error(error);
          document.body.classList.remove("loading");
        });
    }
  })
  .catch((error) => console.error(error));
