"use strict";
const root = document.querySelector("#root");
const baseURL = "https://ajax.test-danit.com/api/json/";

class Request {
  constructor(url) {
    this.url = url;
  }

  get(entity) {
    return fetch(this.url + entity).then((response) => response.json());
  }

  delete(entity, id) {
    return fetch(`${this.url}${entity}/${id}`, { method: "DELETE" });
  }

  post(entity, formData) {
    return fetch(this.url + entity, {
      method: "POST",
      body: JSON.stringify(formData),
    }).then((response) => response.json());
  }

  put(entity, formData) {
    return fetch(this.url + entity, {
      method: "PUT",
      body: JSON.stringify(formData),
    });
  }
}

class Card {
  static #EDIT_ICON = `<svg style="color: rgb(182, 234, 218);" xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16"> 
    <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z" fill="#b6eada"></path> 
    <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z" fill="#b6eada"></path>
</svg>`;

  static #DELETE_ICON = `<svg style="color: rgb(182, 234, 218);" xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-trash" viewBox="0 0 16 16">
    <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z" fill="#b6eada"></path>
    <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z" fill="#b6eada"></path>
</svg>`;

  constructor(title, message, name, email, id, userID, request, target, posts) {
    this.id = id;
    this.userID = userID;
    this.title = title;
    this.message = message;
    this.email = email;

    this.request = request;

    this.name = name.split(" ")[0];
    this.lastName = name.split(" ")[1];
    this.cardElement = Card.#createElement("div", "card");
    this.cardElement.id = id;
    this.target = target;
    this.posts = posts;
  }

  static #createElement(tagName, className, textContent = "", props = []) {
    const element = document.createElement(tagName);
    element.classList.add(className);
    element.textContent = textContent;

    if (props.length) props.forEach(([key, value]) => (element[key] = value));

    return element;
  }

  static #createButton(className, icon, handler) {
    const button = Card.#createElement("div", className);
    button.innerHTML = icon;
    button.addEventListener("click", handler);

    return button;
  }

  static #handleEdit(cardID, userID, target, requests, posts, cardElement) {
    return () => {
      const modal = new Modal("editPostForm", target, requests, posts, {
        cardID,
        userID,
        cardElement,
      });
      modal.render();
    };
  }

  static #handleDelete(request, id, cardElement) {
    return () => {
      if (confirm("Are you really want to delete the card?")) {
        request
          .delete("posts", id)
          .then((response) => {
            if (response.ok) {
              cardElement.remove();
            } else {
              alert(
                response.status +
                  " - This post is not in database! Just refresh the page"
              );
            }
          })
          .catch((error) => alert(error.message));
      }
    };
  }

  #createNameWrap() {
    const wrap = Card.#createElement("div", "nameWrap");
    const name = Card.#createElement("p", "cardName", this.name);
    const lastName = Card.#createElement("p", "cardLastName", this.lastName);

    const editBtn = Card.#createButton(
      "cardEditBtn",
      Card.#EDIT_ICON,
      Card.#handleEdit(
        this.id,
        this.userID,
        this.target,
        this.request,
        this.posts,
        this.cardElement
      )
    );
    const deleteBtn = Card.#createButton(
      "cardDeleteBtn",
      Card.#DELETE_ICON,
      Card.#handleDelete(this.request, this.id, this.cardElement)
    );

    wrap.append(name, lastName, editBtn, deleteBtn);
    return wrap;
  }

  #createInnerWrap() {
    const innerWrap = Card.#createElement("div", "cardInnerWrap");
    const nameWrap = this.#createNameWrap();
    const email = Card.#createElement("p", "cardEmail", this.email);
    const title = Card.#createElement("h2", "cardTitle", this.title);
    const message = Card.#createElement("p", "cardMessage", this.message);
    innerWrap.append(nameWrap, email, title, message);
    return innerWrap;
  }

  render() {
    this.cardElement.append(
      Card.#createElement("img", "cardAvatar", "", [
        [
          "src",
          `https://ui-avatars.com/api/?name=${this.name}+${this.lastName}`,
        ],
      ]),
      this.#createInnerWrap()
    );

    return this.cardElement;
  }
}

class Modal {
  static #CLOSE_ICON = `<svg style="color: rgb(3, 0, 28);" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"> 
           <path d="M15.59 7L12 10.59L8.41 7L7 8.41L10.59 12L7 15.59L8.41 17L12 13.41L15.59 17L17 15.59L13.41 12L17 8.41L15.59 7Z" fill="#03001c"></path> </svg>`;

  constructor(
    formID,
    target,
    request,
    posts,
    { cardID, userID, cardElement } = {}
  ) {
    this.formID = formID;
    this.target = target;
    this.body = document.querySelector("body");
    this.posts = posts;
    this.modal = Modal.#createElement(["div", "modal", ["modal"]], [], target);

    this.request = request;
    this.cardElement = cardElement;
    this.cardID = cardID;
    this.userID = userID;

    if (this.cardElement) {
      this.title = this.cardElement.querySelector(".cardTitle");
      this.text = this.cardElement.querySelector(".cardMessage");
    }
  }

  static #createElement([tagName, id, className], props = [], target) {
    const element = document.createElement(tagName);
    element.id = id;
    element.classList.add(...className);

    if (props.length) props.forEach(([key, value]) => (element[key] = value));

    target.append(element);

    return element;
  }

  #addPostHandler(formData) {
    this.request.get(`users/${this.userID}`).then(({ name, email }) => {
      this.request
        .post("posts", { ...formData, name, email })
        .then(({ name, email, id }) => {
          const card = new Card(
            formData.get("title"),
            formData.get("text"),
            name,
            email,
            id,
            this.userID,
            this.request,
            this.target
          );
          this.posts.prepend(card.render());
        })
        .catch((error) => alert(error.message));
    });
  }

  #editPostHandler(formData) {
    this.request
      .put(`posts/${this.cardID}`, {
        id: this.cardID,
        userId: this.userID,
        title: this.title.textContent,
        body: this.text.textContent,
      })
      .then((response) => {
        if (response.ok) {
          this.title.textContent = formData.get("title");
          this.text.textContent = formData.get("text");
        } else {
          alert(
            response.status +
              " - Sorry, you can't edit this post. It's not in database! Just refresh the page"
          );
        }
      })
      .catch((error) => alert(error.message));
  }

  static #validateFormData(formData, event) {
    if (formData.get("title").trim() === "") {
      return event.target.querySelector("#titleInput");
    }
    if (formData.get("text").trim() === "") {
      return event.target.querySelector("#textInput");
    }

    return "valid";
  }

  #createForm() {
    const form = Modal.#createElement(
      ["form", this.formID, ["form"]],
      [],
      this.modal
    );
    const closeBtn = Modal.#createElement(
      ["div", "closeBtn", ["closeBtn"]],
      [["innerHTML", Modal.#CLOSE_ICON]],
      form
    );

    closeBtn.addEventListener("click", () => {
      this.modal.remove();
      this.body.style["overflow-y"] = "";
    });

    form.addEventListener("submit", (e) => {
      e.preventDefault();

      this.body.style["overflow-y"] = "";

      const formData = new FormData(e.target);

      const res = Modal.#validateFormData(formData, e);

      if (res === "valid") {
        if (this.formID === "addPostForm") {
          this.#addPostHandler(formData);
        } else if (this.formID === "editPostForm") {
          this.#editPostHandler(formData);
        }
        this.modal.remove();
      } else {
        alert("You must enter value!");
        res.focus();
      }
    });

    return form;
  }

  render() {
    this.modal.style.display = "block";
    this.body.style["overflow-y"] = "hidden";

    const form = this.#createForm();
    Modal.#createElement(
      ["input", "titleInput", ["titleInput"]],
      [
        ["value", `${this.cardElement ? this.title.textContent : ""}`],
        ["type", "text"],
        ["name", "title"],
        ["placeholder", "Enter title ..."],
        ["required", true],
      ],
      form
    );
    Modal.#createElement(
      ["textarea", "textInput", ["textInput"]],
      [
        ["value", `${this.cardElement ? this.text.textContent : ""}`],
        ["name", "text"],
        ["placeholder", "Enter message ..."],
        ["required", true],
        ["rows", 5],
      ],
      form
    );
    Modal.#createElement(
      ["button", "createPostBtn", ["submitBtn"]],
      [
        ["type", "submit"],
        ["textContent", "Submit"],
      ],
      form
    );
  }
}

class Twitter {
  constructor(url, target) {
    this.request = new Request(url);
    this.target = target;
  }

  static #createElement([tagName, id, className], props = [], target) {
    const element = document.createElement(tagName);
    element.id = id;
    element.classList.add(className);

    if (props.length) props.forEach(([key, value]) => (element[key] = value));

    target.append(element);

    return element;
  }

  render() {
    const posts = Twitter.#createElement(
      ["div", "posts", "posts"],
      [],
      this.target
    );

    const addCardBtn = Twitter.#createElement(
      ["button", "addCardBtn", "addCardBtn"],
      [
        ["textContent", "Add Post"],
        ["disabled", true],
      ],
      this.target
    );

    addCardBtn.addEventListener("click", () => {
      let userID = Math.floor(Math.random() * 10) + 1;

      const modal = new Modal("addPostForm", this.target, this.request, posts, {
        userID,
      });
      modal.render();
    });
    const loader = Twitter.#createElement(
      ["span", "loader", "loader"],
      [],
      this.target
    );

    Promise.all([this.request.get("users"), this.request.get("posts")])
      .then((data) => {
        loader.remove();
        let [usersArr, postsArr] = data;

        postsArr.forEach(({ id, userId, title, body }) => {
          const { name, email } = usersArr.find(({ id, name, email }) =>
            userId === id ? { name, email } : ""
          );
          const card = new Card(
            title,
            body,
            name,
            email,
            id,
            userId,
            this.request,
            this.target,
            posts
          );

          posts.append(card.render());
        });
        this.target.append(posts);
        addCardBtn.disabled = false;
      })
      .catch((error) => {
        loader.remove();
        alert(error.message);
      });
  }
}

const twitter = new Twitter(baseURL, root);
twitter.render();
